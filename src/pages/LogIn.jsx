import { LoginForm } from "components/registerForm/LoginForm";
import React from "react";
import styled from "styled-components";
import { GoBackButton } from "components/uiPrimitives/GoBackButton";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

function LogIn() {
  return (
    <Wrapper>
      <GoBackButton/>
      <LoginForm />
    </Wrapper>
  );
}

export { LogIn };
