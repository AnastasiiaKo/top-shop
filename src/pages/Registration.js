import React from "react";
import { RegisterForm } from "components/registerForm/RegisterForm";
import styled from "styled-components";
import { GoBackButton } from "components/uiPrimitives/GoBackButton";

const Wrapper = styled.div`
  padding-top: 1rem;
  background-color: var(--main-lightgreen);
  display: flex;
  justify-content: center;
  height: 100%;
`;

function Registration() {
  return (
    <Wrapper>
      <GoBackButton />
      <RegisterForm />
    </Wrapper>
  );
}

export { Registration };
