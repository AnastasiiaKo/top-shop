import React from "react";
import { getPopular } from "apis/movies/index";
import { MovieListContainer } from "components/MovieListContainer/index";

function Popular() {
    // window.onscroll = function () {
    //   var e = document.getElementById("scrolltop");
    //   if (!e) {
    //     e = document.createElement("a");
    //     e.id = "scrolltop";
    //     e.href = "#";
    //     document.body.appendChild(e);
    //   }
    //   e.style.display =
    //     document.documentElement.scrollTop > 200 ? "block" : "none";
    //   e.onclick = (ev) => {
    //     ev.preventDefault();
    //     document.documentElement.scrollTop = 0;
    //   };
    // };
  return <MovieListContainer method={getPopular} />;
}
export { Popular };
