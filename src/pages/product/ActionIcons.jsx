import React, { useState } from "react";
import styled from "styled-components";
import { GenreList } from "pages/product/Genres";
import { ProgressBar } from "components/uiPrimitives/ProgressBar";
import { Tooltip as ReactTooltip } from "react-tooltip";
import { BsFillHeartFill } from "react-icons/bs";
import { FaList } from "react-icons/fa6";
import { AiFillStar } from "react-icons/ai";
import PropTypes from "prop-types";
import { Rate } from "pages/product/Rate";
import { getIsLogedIn } from "state/selectors/users";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Button } from "components/uiPrimitives/Button";
import { FaPlay } from "react-icons/fa";
import { Trailer } from "components/Trailer";

const Wrapper = styled(GenreList)`
  margin-bottom: 45px;
  size: 18px;
`;
const IconContainer = styled.div`
  background-color: var(--color-green);
  border-radius: 50%;
  padding: 15px;
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 5px;
  cursor: pointer;
  color: var(--main-white);
  &:hover,
  &:focus,
  &.active {
    background-color: rgba(23, 175, 38, 1);
    color: white;
    box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.5), 0 0 15px rgba(0, 0, 0, 0.5),
      0 0 25px rgba(0, 0, 0, 0.5);
  }
`;
const Icon = styled(FaPlay)`
  margin-right: 5px;
`;
const PlayButton = styled(Button)`
  max-width: 8rem;
  min-width: 6rem;
  margin-left: 2rem;
`;

const ActionIconsList = [
  {
    name: "addFavorite",
    title: "Mark as favorite",
    icon: <BsFillHeartFill />,
  },
  {
    name: "addWatchlist",
    title: "Add to watchlist",
    icon: <FaList />,
  },
  {
    name: "rate",
    title: "Rate",
    icon: <AiFillStar />,
  },
];
const Rating = styled.div`
  width: 70px;
  height: 70px;
  margin-right: 45px;
  font-size: small;
  font-weight: bold;
`;

function ActionIcons({ rating }) {
  const [isOpenRate, setIsOpenRate] = useState(false);
  const[isOpenTrailer, setIsOpenRateTrailer] = useState(false);
  const isLoggedIn = useSelector(getIsLogedIn);
  const navigate = useNavigate();

  const handleClick = () => {
    if (isLoggedIn) {
      setIsOpenRate(!isOpenRate);
    } else
      navigate("/logIn", {
        replace: true,
        state: { backPath: location.pathname },
      });
  };
  const handleClickTrailer = () => {
    setIsOpenRateTrailer(!isOpenTrailer);
  };
  return (
    <Wrapper>
      <Rating>
        <ProgressBar rating={rating} />
        <p>User score</p>
      </Rating>
      {ActionIconsList.map(({ name, icon, title }) => (
        <IconContainer
          key={name}
          id={name}
          onClick={handleClick}
          data-tooltip-id={name}
          data-tooltip-content={title}
        >
          {icon}
          <ReactTooltip id={name} type="warning" effect="solid">
            <span>{title}</span>
          </ReactTooltip>
        </IconContainer>
      ))}
      {isOpenRate && <Rate handleClick={handleClick} />}
      <PlayButton onClick={handleClickTrailer}>
        <Icon />
        <>Trailer</>
      </PlayButton>
      {isOpenTrailer && <Trailer handleClick={handleClickTrailer} />}
    </Wrapper>
  );
}

ActionIcons.propTypes = {
  rating: PropTypes.number,
  name: PropTypes.string,
};

export { ActionIcons };
