import React from "react";
import styled from "styled-components";
import { pathBoard } from "path";
import { NavLink, Outlet, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { getDetailsById } from "apis/movies/index";
import { Poster } from "pages/product/Poster";
import { Genres } from "pages/product/Genres";
import { ActionIcons } from "pages/product/ActionIcons";
import { GoBackButton } from "components/uiPrimitives/GoBackButton";

const Wrapper = styled.section`
  padding: 1rem 4.5rem;
  display: flex;
`;
const ContantContainer = styled.div`
  width: 60%;
  padding-left: 2.5rem;
`;
const ChildrenContainer = styled.div`
  display: flex;
  margin-top: 20px;
  margin-bottom: 20px;

  border-top: 2px solid var(--border-color);
  border-bottom: 2px solid var(--border-color);
  padding-top: 10px;
  padding-bottom: 10px;
`;
const NestedLink = styled(NavLink)`
  margin-right: 20px;
  text-decoration: none;
  color: var(--color-black);
  &:hover,
  &:focus,
  &.active {
    text-decoration: underline;
    color: var(--main-darkgreen);
  }
`;

function Product() {
  const params = useParams();
  const movieId = params.productId;
  const nestedLink = [
    {
      name: "Actors",
      path: `${pathBoard.PRODUCT}/${movieId}/actors`,
    },
    {
      name: "Review",
      path: `${pathBoard.PRODUCT}/${movieId}/reviews`,
    },
  ];

  const [movieDetails, setMovieDetails] = useState({});
  async function getMovieDetails(id) {
    await getDetailsById(id).then((response) => setMovieDetails(response));
  }

  useEffect(() => {
    getMovieDetails(movieId);
  }, []);
  const {
    poster_path,
    title,
    release_date,
    genres,
    runtime,
    overview,
    vote_average,
  } = movieDetails;

  return (
    <Wrapper>
      <GoBackButton />
      <Poster path={poster_path} />
      <ContantContainer>
        {title && (
          <h1>
            {title} {release_date && `(${release_date.slice(0, 4)})`}
          </h1>
        )}
        <Genres genres={genres} runtime={runtime} />
        <ActionIcons rating={vote_average} />
        <h3>Overview</h3>
        <p>{overview}</p>
        <ChildrenContainer>
          {nestedLink.map(({ name, path }) => (
            <NestedLink key={name} to={path}>
              {name}
            </NestedLink>
          ))}
        </ChildrenContainer>
        <Outlet />
      </ContantContainer>
    </Wrapper>
  );
}

export { Product };
