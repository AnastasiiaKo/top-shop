import React from "react";
import styled from "styled-components";
import { useState } from "react";
import { Modal } from "components/uiPrimitives/Modal";
import PropTypes from "prop-types";
import { Tooltip as ReactTooltip } from "react-tooltip";
import { addRating } from "apis/rating";
import { useParams, useNavigate, useLocation } from "react-router-dom";
import { getIsLogedIn } from "state/selectors/users";
import { useSelector } from "react-redux";
import { getUserSessionId } from "state/selectors/users";

const StarList = styled.input`
  display: none;
  justify-content: center;
  align-items: center;
  align-content: center;
  margin-left: 12px;
  max-width: 10px;
`;
const Star = styled.span`
  cursor: pointer;
  font-size: 2.5rem;
  margin: 5px;
  position: relative;
  color: ${(props) => props.color};
  width: 10px;
`;
const Rating = styled.span`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 15px;
  height: 15px;
  font-size: small;
  color: var(--main-darkgreen);
  color: rgb(162, 218, 8);
`;
const Thanks = styled.div`
  font-size: 32px;
  font-weight: bolder;
  color: var(--main-darkgreen);
`;
const totalStars = [
  {
    star: 1,
    color: "rgb(63, 2, 2)",
    title: "Disaster",
  },
  {
    star: 2,
    color: "rgb(154, 3, 3)",
    title: "Awufull",
  },
  {
    star: 3,
    color: "rgb(218, 57, 8)",
    title: "Truly bad",
  },
  {
    star: 4,
    color: "rgb(222, 130, 37)",
    title: "Not recommended",
  },
  {
    star: 5,
    color: "rgb(237, 206, 7)",
    title: "Doesn't worth your time",
  },
  {
    star: 6,
    color: "rgb(180, 220, 19)",
    title: "Could be better",
  },
  {
    star: 7,
    color: "rgb(113, 210, 44)",
    title: "Good",
  },
  {
    star: 8,
    color: "rgb(48, 118, 7)",
    title: "Really good",
  },
  {
    star: 9,
    color: "rgb(3, 108, 24)",
    title: "Wonderfull",
  },
  {
    star: 10,
    color: "rgb(3, 93, 31)",
    title: "Awesome",
  },
];

function Rate({ handleClick }) {
  const [rating, setRating] = useState();
  const [hover, setHover] = useState(null);
  const [isOpen, setIsOpen] = useState(false);
  const param = useParams();
  const movieId = param.productId;
  const isLoggedIn = useSelector(getIsLogedIn);
  const sessionId = useSelector(getUserSessionId);
  const location = useLocation();
  let navigate = useNavigate();

  const onRate = (star) => {
    if (isLoggedIn) {
      addRating(movieId, sessionId, star);
      setIsOpen(true);
      setTimeout(handleClick, 2000);
    } else
      navigate("/logIn", {
        state: { backPath: `${location.pathname}?isRateModalOpen=true` },
      });
  };
  return (
    <Modal
      title="What is your impression?"
      handleClick={handleClick}
      theme="whiteTheme"
    >
      {isOpen ? (
        <Thanks>Thanks for your feedback!</Thanks>
      ) : (
        totalStars.map(({ star, color, title }) => {
          const currentRating = star;
          return (
            <label key={star}>
              <StarList
                type="radio"
                name="rating"
                value={currentRating}
                onChange={() => setRating(currentRating)}
              />
              <Star
                color={currentRating <= (hover || rating) ? color : "#e4e5e9"}
                onMouseEnter={() => setHover(currentRating)}
                onMouseLeave={() => setHover(null)}
                id={color}
                data-tooltip-id={color}
                data-tooltip-content={title}
                value={star}
                onClick={() => onRate(currentRating)}
              >
                <Rating></Rating>&#9733;
              </Star>
              <ReactTooltip id={color} effect="solid">
                <span>{title}</span>
              </ReactTooltip>
            </label>
          );
        })
      )}
    </Modal>
  );
}
Rate.propTypes = {
  handleClick: PropTypes.func,
};
export { Rate };
