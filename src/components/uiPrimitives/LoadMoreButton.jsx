import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Button } from "./Button";

const LoadButton = styled(Button)`
  box-sizing: border-box;
  max-width: 500px;
  min-width: 40px;
  min-height: 35px;
  width: 100%;
  padding: 10px;
  margin-top: 20px;
  background-color: var(--main-darkgreen);
  padding-block: 0;
  padding-inline: 0;


`;

function LoadMoreButton({ onClick }) {
  return <LoadButton onClick={onClick}>Load more</LoadButton>;
}

LoadMoreButton.propTypes = {
  onClick: PropTypes.func,
};

export { LoadMoreButton };
