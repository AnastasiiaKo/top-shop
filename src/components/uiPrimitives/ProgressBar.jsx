import React from "react";
import PropTypes from "prop-types";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";


function ProgressBar({ rating }) {
  const movieRating = rating?.toFixed(1);
  return (
    <CircularProgressbar
      values={[0, 20, 40, 60, 80, 100]}
      value={Math.round(movieRating * 10)}
      text={movieRating}
      background
      backgroundPadding={6}
      styles={buildStyles({
        textColor: "white",
        textSize: "30px",
        pathColor: "var(--main-white)",
        backgroundColor: "var(--color-green)",
        trailColor: "transparent",
      })}
      strokeWidth={7}
    />
  );
}
ProgressBar.propTypes = {
  rating: PropTypes.number,
};

export { ProgressBar };
