import React from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { MdOutlineKeyboardDoubleArrowLeft } from "react-icons/md";
import { Button } from "./Button";

const BackButton = styled(Button)`
  width: 35px;
  height: 35px;
  background-color: var(--main-white);
  position: absolute;
  left: 2%;
  top: 20%;
`;
const Icon = styled(MdOutlineKeyboardDoubleArrowLeft)`
  color: var(--main-darkgreen);
  &:hover {
    color: var(--main-white);
  }
`;

function GoBackButton() {
  const navigate = useNavigate();
  return (
    <BackButton onClick={() => navigate(-1)}>
      <Icon size="1.5rem" />
    </BackButton>
  );
}
export { GoBackButton };
