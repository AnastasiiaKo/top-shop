import React from "react";
import styled from "styled-components";

const Menu = styled.ul`
  position: absolute;
  top: 60px;
  right: 75px;
  list-style: none;
  width: 150px;
  font-size: 14px;
  font-weight: normal;
  border-radius: 15px;
  padding: 0px;
`;
const MenuItem = styled.li`
  height: 15px;
  padding: 10px;
  border-bottom: 1px solid var(--main-white);
  border-radius: 7px;
  color: var(--main-white);
  background-color: var(--main-darkgreen);
  text-align: left;
  cursor: pointer;
  &:hover,
  &:focus,
  &.active {
    background-color: var(--color-green);
    color: var(--main-white);
    box-shadow: inset 0 0 5px var(--box-shadow), 0 0 15px var(--box-shadow),
      0 0 25px var(--box-shadow);
    border: none;
  }
`;

const menuItems = [{ name: "Rated movies" }, { name: "Favorite list" }];

function DropMenu() {
  return (
    <Menu>
      {menuItems.map((item) => (
        <MenuItem key={item.name}>{item.name}</MenuItem>
      ))}
    </Menu>
  );
}
export { DropMenu };
