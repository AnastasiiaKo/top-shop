import React, { useEffect } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { IoCloseSharp } from "react-icons/io5";
import { ThemeProvider } from "styled-components";

const darkTheme = {
  width: "840px",
  height: "540px",
  backgroundColor: "var(--color-black)",
  color: "var(--main-white)",
};
const whiteTheme = {
  width: "740px",
  height: "240px",
  backgroundColor: "var(--main-white)",
  border: "1.5rem",
  color: "var(--main-darkgreen)",
};

const ContentBox = styled.div`
  width: ${(props) => props.theme.width};
  height: ${(props) => props.theme.height};
  background-color: ${(props) => props.theme.backgroundColor};
  border-radius: ${(props) => props.theme.border};
  position: fixed;
  float: left;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;
const Wrapper = styled(ContentBox)`
  width: 100%;
  height: 100%;
  background-color: var(--box-shadow);
  z-index: 100089;
  display: flex;
`;
const HeadLine = styled.div`
  color: ${(props) => props.theme.color};
  width: 100%;
  height: 55px;
  display: flex;
  justify-content: space-between;
  background-color: ${(props) => props.theme.backgroundColor};
  align-items: center;
  border-radius: ${(props) => props.theme.border};
`;
const Title = styled.h1`
  margin: 15px;
  margin-left: 35px;
  padding-top: 20px;
`;
const DismissButton = styled.div`
  display: flex;
  justify-content: center;
  cursor: pointer;
  margin: 10px;
  width: 26px;
  height: 26px;
  align-items: center;
  &:hover,
  &:focus,
  &.active {
    color: ${(props) => props.theme.backgroundColor};
    background-color: var(--color-grey);
    border: 1px solid ${(props) => props.theme.backgroundColor};
    border-radius: 50%;
    background-color: ${(props) => props.theme.color};
    box-shadow: inset 0 0 5px var(--box-shadow), 0 0 15px var(--box-shadow),
      0 0 25px var(--box-shadow);
  }
`;
const Content = styled.div`
  text-align: center;
  align-content: center;
  margin-top: 40px;
`;

function Modal({ children, title, handleClick, theme }) {
  const getTheme = (themeColor) => {
    if (themeColor === "whiteTheme") {
      return whiteTheme;
    } else if (themeColor === "darkTheme") {
      return darkTheme;
    }
  };
  const handleEscape = (e) => {
    if (e.keyCode === 27) {
      handleClick();
    }
  };

  useEffect(() => {
    document.addEventListener("keydown", handleEscape);

    return () => document.removeEventListener("keydown", handleEscape);
  }, []);

  return (
    <Wrapper>
      <ThemeProvider theme={getTheme(theme)}>
        <ContentBox>
          <HeadLine>
            <Title>{title}</Title>
            <DismissButton onClick={handleClick}>
              <IoCloseSharp color={theme.color} />
            </DismissButton>
          </HeadLine>
          <Content>{children}</Content>
        </ContentBox>
      </ThemeProvider>
    </Wrapper>
  );
}
Modal.propTypes = {
  children: PropTypes.any,
  title: PropTypes.string,
  handleClick: PropTypes.func,
  theme: PropTypes.string,
};
export { Modal };
