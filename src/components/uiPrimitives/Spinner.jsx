import React from "react";
import { Oval } from "react-loader-spinner";

function Spinner() {
  return (
    <Oval
      visible={true}
      height="80"
      width="80"
      color="var(--color-green)"
      strokeWidth="4"
      ariaLabel="oval-loading"
      wrapperStyle={{}}
      wrapperClass=""
    />
  );
}
export { Spinner };
