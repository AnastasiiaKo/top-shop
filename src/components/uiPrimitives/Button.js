import styled from "styled-components";

const Button = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid var(--border-color);
  border-radius: 50px;
  padding-block: 0;
  padding-inline: 0;
  cursor: pointer;
  width: 10rem;
  height: 3rem;
  background-color: var(--color-green);
  color: var(--main-white);
  font-size: 16px;
  font-weight: bold;
  transition: all 500ms ease;
  &:hover,
  &:focus,
  &.active {
    background-color: var(--color-green);
    color: var(--main-white);
    box-shadow: inset 0 0 5px var(--box-shadow), 0 0 15px var(--box-shadow),
      0 0 25px var(--box-shadow);
    border: none;
  }
`;
export { Button };
