import React from "react";
import { ReadMore } from "components/Review/ReadMore";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ImStarFull } from "react-icons/im";

const TextContainer = styled.div`
  font-size: 0.8rem;
  color: var(--color-black);
`;
const Title = styled.h3`
  font-size: 0.8rem;
  font-weight: bold;
  margin-right: 10px;
  color: var(--main-darkgreen);
`;
const TitleContainer = styled.div`
  display: flex;
  align-items: center;
`;
const Raiting = styled.div`
  font-size: 0.8rem;
  font-weight: bold;
  color: var(--main-darkgreen);
  background-color: var(--main-lightgreen);
  width: 50px;
  height: 15px;
  border-radius: 25px;
  padding: 5px;
`;
const Star = styled.span`
  margin-right: 8px;
`;
function ReviewTextContainer({ id, author, rating, content }) {
  return (
    <TextContainer>
      <TitleContainer>
        <Title>{author}</Title>
        {rating && (
          <Raiting>
            <Star>
              <ImStarFull color={"gold"} />
            </Star>
            {rating}
          </Raiting>
        )}
      </TitleContainer>
      <ReadMore id={id} text={content}></ReadMore>
    </TextContainer>
  );
}

ReviewTextContainer.propTypes = {
  id: PropTypes.string,
  author: PropTypes.string,
  rating: PropTypes.number,
  content: PropTypes.string,
};

export { ReviewTextContainer };
