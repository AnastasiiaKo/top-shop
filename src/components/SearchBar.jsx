import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import magnifier from "assets/headerIcons/magnifier.svg";
import { pathBoard } from "path";
import { Button } from "./uiPrimitives/Button";

const Form = styled.form`
  height: 1.9rem;
  display: flex;
  width: fit-content;
  padding-left: 1rem;
`;
export const Input = styled.input`
  border: 1px solid var(--border-color);
  border-radius: 25px;
  width: max-content;
  min-width: 270px;
  height: 100%;
  color: var(--color-grey);
  padding-left: 24px;
  padding-right: 24px;
  margin-right: 8px;
  cursor: pointer;
  &:hover,
  &:focus {
    border: 1px solid var(--color-green);
    outline: none;
  }
`;
const SearchButton = styled(Button)`
  width: 2rem;
  height: 2rem;
  padding-block: 0;
  padding-inline: 0;
`;
const Magnifier = styled.img`
  width: 0.8rem;
  height: 2rem;
`;

function SearchBar() {
  const [movieName, setMovieName] = useState("");
  const navigate = useNavigate();

  const handleChange = (e) => {
    const value = e.currentTarget.value.toLowerCase();
    setMovieName(value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    navigate(`${pathBoard.search}/${movieName}`);
    setMovieName("");
  };
  return (
    <Form onSubmit={handleSubmit}>
      <Input
        placeholder="Search"
        onChange={handleChange}
        value={movieName}
      ></Input>
      <SearchButton type="submit">
        <Magnifier src={magnifier} alt="Search button" />
      </SearchButton>
    </Form>
  );
}

export { SearchBar };
