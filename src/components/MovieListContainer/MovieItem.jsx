import React from "react";
import styled from "styled-components";
import { ProgressBar } from "components/uiPrimitives/ProgressBar";
import { Link } from "react-router-dom";
import { pathBoard } from "path";
import PropTypes from "prop-types";
import { FcGallery } from "react-icons/fc";

const Wrapper = styled.div`
  margin-right: 45px;
  font-size: small;
  font-weight: bold;
  width: 40px;
  height: 40px;
  position: absolute;
  bottom: -15px;
  left: 7px;
`;

const ItemContainer = styled(Link)`
  list-style: none;
  display: block;
  margin: 5px 10px;
  border-radius: 15px;
  background-color: var(--border-color);
  border: 1px solid var(--border-color);
  padding-bottom: 10px;
  min-width: 140px;
  width: 180px;
  height: 340px;
  background-color: var(--main-white);
  box-shadow: 0 2px 8px var(--box-shadow);
  cursor: pointer;
  text-decoration: none;
  color: var(--color-black);
  transition: all  .125s ease;
  overflow: hidden;
  &:hover,
  &:focus {
    transform: scale(1.025);
  }
`;

const ImgContainer = styled.div`
  height: 260px;
  width: 100%;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: var(--main-lightgreen);
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
`;

const TextName = styled.p`
  font-size: small;
  font-weight: bold;
  margin-top: 10px;
  padding: 5px;
  text-align: center;
`;

const TextData = styled.p`
  text-align: center;
  margin-bottom: 5px;
  color: var(--color-grey);
  font-size: 14px;
  font-family: Lexend;
  line-height: 21px;
  letter-spacing: 0px;
`;

const MovieItem = React.memo(({ movie }) => {
  const { id, poster_path, title, release_date, vote_average } = movie;
  return (
    <ItemContainer to={`${pathBoard.PRODUCT}/${id}`}>
      <ImgContainer>
        {poster_path ? (
          <Img
            src={`https://image.tmdb.org/t/p/w500/${poster_path}`}
            alt={title}
          />
        ) : (
          <FcGallery size={70} color="rgba(5, 66, 44, 1)" />
        )}
        <Wrapper>
          <ProgressBar rating={vote_average} />
        </Wrapper>
      </ImgContainer>
      <div>
        <TextName>{title}</TextName>
        <TextData>
          {release_date && `(${release_date.split("-")[0]})`}
        </TextData>
      </div>
    </ItemContainer>
  );
});
MovieItem.displayName = "MovieItem";

MovieItem.propTypes = {
  movie: PropTypes.shape({
    id: PropTypes.number.isRequired,
    poster_path: PropTypes.string,
    title: PropTypes.string.isRequired,
    release_date: PropTypes.string.isRequired,
    vote_average: PropTypes.number.isRequired,
  }).isRequired,
};
export { MovieItem };
