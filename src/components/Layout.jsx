import React from "react";
import { Outlet } from "react-router-dom";
import { Header } from "components/Header";
import { Footer } from "components/Footer";
import styled from "styled-components";

const Main = styled.main`
  flex: 1;
  margin-top: 7.75rem;
  overflow-y: auto;
  background-color: var(--main-lightgreen);
`;
const LayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  overflow: hidden;
`;

function Layout() {
  return (
      <LayoutContainer>
        <Header />
        <Main>
          <Outlet />
        </Main>
        <Footer />
      </LayoutContainer>
  );
}
export { Layout };
