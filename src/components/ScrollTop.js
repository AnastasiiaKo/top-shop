import React from "react";
import { useEffect, useState } from "react";
import { MdOutlineKeyboardDoubleArrowUp } from "react-icons/md";
import styled from "styled-components";

const Button = styled.button`
  background-color: var(--color-green);
  position: fixed;
  bottom: 150px;
  right: 50px;
  width: 50px;
  height: 50px;
  border: 1px solid var(--border-color);
  border-radius: 50%;
  z-index: 999;
  transition: all 500ms ease;
  &:hover,
  &:focus,
  &.active {
    background-color: var(--color-green);
    color: var(--main-white);
    box-shadow: inset 0 0 5px var(--box-shadow), 0 0 15px var(--box-shadow),
      0 0 25px var(--box-shadow);
  }
`;
const Icon = styled(MdOutlineKeyboardDoubleArrowUp)`
  color: var(--main-white);
`;

function ScrollToTop() {
  const [backToTop, setBackToTop] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 50) {
        setBackToTop(true);
      } else {
        setBackToTop(false);
      }
    });
  }, []);
  const scrollUp = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <div>
      {backToTop && (
        <Button onClick={scrollUp}>
          <Icon size='30'/>
        </Button>
      )}
    </div>
  );
}
export { ScrollToTop };
