import React, {useState} from "react";
import styled from "styled-components";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { SearchBar } from "components/SearchBar";
import { pathBoard } from "path";
import { FcVideoCall } from "react-icons/fc";
import { getIsLogedIn, getCurrentUserName } from "state/selectors/users";
import { MainMenu } from "components/MainMenu";
import { useDispatch, useSelector } from "react-redux";
import Hamburger from "hamburger-react";
import { Button } from "./uiPrimitives/Button";
import { toggleIsLoggedIn } from "state/auth/registerSlice";
import { DropMenu } from "components/uiPrimitives/DropMenu";

const Wrapper = styled.div`
  font-family: "Lexend", sans-serif;
  background-color: var(--main-white);
  position: fixed;
  top: 0;
  left: 0;
  height: 7.75rem;
  width: 100%;
  z-index: 1000;
`;
const OfferContainer = styled.div`
  background-color: var(--main-darkgreen);
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  height: 1.8 rem;
  width: 100%;
`;
const OfferText = styled.p`
  margin: 0;
  color: var(--main-white);
  font-weight: 400;
  font-size: 0.7rem;
  width: 100%;
  padding-top: 0.4rem;
  padding-bottom: 0.4rem;
`;
const MiddleLayer = styled.div`
  height: 1.9rem;
  padding: 14px 64px;
  display: flex;
  justify-content: space-between;
  text-align: center;
  vertical-align: middle;
`;
const AutorizationButton = styled(Button)`
  width: 8rem;
  height: 2rem;
  font-weight: normal;
  font-size: 15px;
`;
const Account = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;


function Header() {
  const isLoggedIn = useSelector(getIsLogedIn);
  const userName = useSelector(getCurrentUserName);
  const [isOpen, setOpen] = useState(false);
  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useDispatch();
  return (
    <Wrapper>
      <OfferContainer>
        <OfferText>Find your movie among millions here...</OfferText>
      </OfferContainer>
      <MiddleLayer>
        <Link to={pathBoard.home}>
          <FcVideoCall size={30} />
        </Link>
        <SearchBar />
        {isLoggedIn ? (
          <Account>
            <p>{userName}</p>
            <Hamburger
              toggled={isOpen}
              toggle={setOpen}
              size={20}
              color="rgba(5, 66, 44, 1)"
            />
            {isOpen && <DropMenu />}  
            <AutorizationButton
              onClick={() => dispatch(toggleIsLoggedIn(false))}
            >
              Log out
            </AutorizationButton>
          </Account>
        ) : (
          <AutorizationButton
            onClick={() => navigate(pathBoard.authentication, {state:{backPath:`${location.pathname}`}})}
          >
            Log in
          </AutorizationButton>
        )}
      </MiddleLayer>
      <MainMenu />
    </Wrapper>
  );
}
export { Header };
