import React from "react";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { pathBoard } from "path";
import { InputForm } from "components/registerForm/InputForm";
import { addCurrenUser, toggleIsLoggedIn } from "state/auth/registerSlice";
import { useSelector } from "react-redux";
import { getUsers } from "state/selectors/users";

const defaultForm = {
  email: "",
  password: "",
};

function LoginForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  console.log(location.state.backPath);
  const users = useSelector(getUsers);
  
  const backPath = location.state?.backPath ?? "/";
  console.log("BackPath:", backPath);

  const isUser = (email, password) => {
    const currentUser = users.find(
      (user) => user.email === email && user.password === password
    );
    return currentUser;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    const userData = Object.fromEntries(formData);
    const currentUser = isUser(userData.email, userData.password);
    if (currentUser) {
      dispatch(addCurrenUser(currentUser));
      dispatch(toggleIsLoggedIn(true));
      navigate(backPath);
    } else {
      alert("There are no users with such email and password");
    }
  };
  return (
    <InputForm
      params={Object.keys(defaultForm)}
      handleSubmit={handleSubmit}
      title="Log in"
      button="Log in"
      questionText="Are you new to moviesearcher?"
      authorizationType="Create an account"
      link={pathBoard.registration}
      backPath={backPath}
    />
  );
}

export { LoginForm };
