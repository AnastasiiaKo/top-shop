import { requstMethods } from "apis/requestMethods";

const apiKey = process.env.REACT_APP_API_KEY;
const authorization = process.env.AUTHORIZATION;
const apiUrl = "https://api.themoviedb.org/3/authentication/guest_session/new";

export async function getSessionId() {
      const options = {
        method: requstMethods.GET,
        headers: {
          accept: "application/json",
          Authorization: authorization,
        },
      };
    try {
      const response = await fetch(`${apiUrl}?api_key=${apiKey}`, options);
        if (!response.ok) {
          throw new Error("Response network was not ok");
        }
        const data = await response.json();
        return data.guest_session_id;
    }
    catch(error) {
        console.log(error);
        console.error(error);
        throw error;
    }
}