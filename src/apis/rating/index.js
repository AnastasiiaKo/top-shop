import { requstMethods } from "apis/requestMethods";

const apiKey = process.env.REACT_APP_API_KEY;
const authorization = process.env.AUTHORIZATION;
const apiUrl = "https://api.themoviedb.org/3/movie";

export async function addRating(movieId, sessionId, star) {
  const options = {
    method: requstMethods.POST,
    headers: {
      accept: "appli cation/json",
      "Content-Type": "application/json;charset=utf-8",
      Authorization: authorization,
    },
    body: JSON.stringify({ value: star }),
  };
  try {
    const response = await fetch(
      `${apiUrl}/${movieId}/rating?guest_session_id=${sessionId}&api_key=${apiKey}`,
      options
    );
    if (!response.ok) {
      throw new Error("Network response is not ok");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    console.error(error);
    throw error;
  }
}
