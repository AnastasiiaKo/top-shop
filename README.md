# MOVIESEARCHER

### Description

---

Moviesearcher is a movie search site. Here you can find movies for every taste and also stay updated with upcoming movies as we use the most up-to-date database.
It was created to improve the skills of beginning React developers.

> Note: During the development process, we used open moviedatabase [themoviedb.org API](https://www.themoviedb.org/).

### Environment

---

```sh
node: 20.10.0
npm: 10.2.3
```

### Installation

---

Clone project

```sh
git clone git@gitlab.com:AnastasiiaKo/top-shop.git
```

Install dependencies

```sh
npm ci
```

### Usage

---

Run project

```sh
npm start
```
